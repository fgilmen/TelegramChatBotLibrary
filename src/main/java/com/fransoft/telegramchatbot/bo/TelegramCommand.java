package com.fransoft.telegramchatbot.bo;

import com.fransoft.telegramchatbot.utils.CommandLineUtils;
import java.util.Objects;
import javax.annotation.Nullable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NonNull;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.StringUtils;
import org.telegram.telegrambots.meta.api.methods.send.SendDice;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

/**
 * @author fgilmen@gmail.com
 *
 */
@lombok.Getter
@Builder
@AllArgsConstructor
public class TelegramCommand {

    private boolean command = false;
    private String verb = null;
    private CommandLine params = null;
    private String chatId;
    private Long userId;
    
    public static TelegramCommand build(Update update, @Nullable Options options) throws ParseException {
    	return new TelegramCommand(update, options);
    }
       
    public static TelegramCommand build(Update update) {
    	try {
    		return new TelegramCommand(update, null);
    	} catch (ParseException ignore) {
    	    // Can never happen
    		throw new IllegalStateException(ignore);
        }
    }
    
    private TelegramCommand(Update update, @Nullable Options options) throws ParseException {
        String text = getData(update);
        if (StringUtils.isNotBlank(text)){
            String[] trozos = text.split(" ");
            if (trozos[0].startsWith("/")){
                this.command = true;
                this.verb = trozos[0].replace("/", "");
                if (Objects.nonNull(options)){
                    this.params = CommandLineUtils.getCommandLine(text, options);
                }
            }
        }
    }

    private String getData(@NonNull Update update) {
        String text;
        var msg = update.getMessage();
        if (Objects.isNull(msg)) {
        	msg = (Message) update.getCallbackQuery().getMessage();
            text = update.getCallbackQuery().getData();
        } else {
            text = msg.getText();
        }
        this.chatId = String.valueOf(msg.getChatId());
        this.userId = msg.getFrom().getId();
        return text;
    }


    public SendMessage sendMessage(String text, boolean html){
        var sendMessage = new SendMessage();
        sendMessage.setChatId(this.getChatId());
        sendMessage.setText(text);
        sendMessage.enableHtml(html);
        return sendMessage;
    }

    public SendDocument sendDoc(InputFile document){
        var sendDocument = new SendDocument();
        sendDocument.setChatId(this.getChatId());
        sendDocument.setDocument(document);
        return sendDocument;
    }

    public SendDice sendDice(String emoji){
        var sendDice = new SendDice();
        sendDice.setChatId(this.getChatId());
        if (StringUtils.isNotBlank(emoji)) {
            sendDice.setEmoji(emoji);
        }
        return sendDice;
    }

}

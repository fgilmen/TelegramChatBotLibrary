package com.fransoft.telegramchatbot.bo;

import org.apache.commons.cli.Options;

/**
 * @author fgilmen@gmail.com
 *
 */
@lombok.Getter
@lombok.Builder
public class CommandData {
	
    private Options options;
    private String presentation;
    private String verb;

}

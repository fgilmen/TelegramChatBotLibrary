package com.fransoft.telegramchatbot.exception;

/**
 * @author jorge.delolmo@sngular.com
 *
 */
public class TelegramException extends RuntimeException {

  private static final long serialVersionUID = 19876328767283L;
  

  public TelegramException(String message) {
      super(message);
  }


  public TelegramException(String message, Throwable cause) {
      super(message, cause);
  }
}

package com.fransoft.telegramchatbot.handle.impl;

import com.fransoft.telegramchatbot.handle.ITelegramBootErrorHandler;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Slf4jErrTelegramBootErrorHandler implements ITelegramBootErrorHandler {
	
	public void handleError(Exception ex) {
		if (Objects.nonNull(ex)) {
			log.error(ex.getMessage());
		}
	}
}

package com.fransoft.telegramchatbot.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Objects;
import java.util.StringTokenizer;
import javax.annotation.Nullable;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.StringUtils;

/**
 * @author fgilmen@gmail.com
 *
 */
@lombok.experimental.UtilityClass
public class CommandLineUtils {

    public CommandLine getCommandLine(String strCommands, Options options) throws ParseException {
        CommandLineParser parser = new DefaultParser();
        return parser.parse(options, translateCommandline(strCommands));
    }

    public String getHelpCommandLine(String commandVerb, @Nullable Options options, @Nullable String header){
        var buff = new StringWriter();
        var out = new PrintWriter(buff);
        var formatter = new HelpFormatter();

        formatter.printHelp(out, 160, commandVerb, header, (Objects.isNull(options) ? new Options() : options), 10, 10, null);
        return buff.toString();
    }
    
    /**
     * Crack a command line.
     * 
     * @param toProcess the command line to process.
     * @return the command line broken into strings.
     * An empty or null toProcess parameter results in a zero sized array.
     */
    private String[] translateCommandline(String toProcess) throws ParseException { // NOSONAR Copy from common-weaver-antlib.Commandline
        if (StringUtils.isBlank(toProcess)) {
            return new String[0];
        }

        final var normal = 0;
        final var inQuote = 1;
        final var inDoubleQuote = 2;
        int state = normal;
        final var tok = new StringTokenizer(toProcess, "\"\' ", true);
        final var result = new ArrayList<String>();
        final var current = new StringBuilder();
        var lastTokenHasBeenQuoted = false;

        while (tok.hasMoreTokens()) {
            String nextTok = tok.nextToken();
            switch (state) {
            case inQuote:
                if ("\'".equals(nextTok)) {
                    lastTokenHasBeenQuoted = true;
                    state = normal;
                } else {
                    current.append(nextTok);
                }
                break;
            case inDoubleQuote:
                if ("\"".equals(nextTok)) {
                    lastTokenHasBeenQuoted = true;
                    state = normal;
                } else {
                    current.append(nextTok);
                }
                break;
            default:
                if ("\'".equals(nextTok)) {
                    state = inQuote;
                } else if ("\"".equals(nextTok)) {
                    state = inDoubleQuote;
                } else if (" ".equals(nextTok)) {
                    if (lastTokenHasBeenQuoted || current.length() != 0) {
                        result.add(current.toString());
                        current.setLength(0);
                    }
                } else {
                    current.append(nextTok);
                }
                lastTokenHasBeenQuoted = false;
                break;
            }
        }
        if (lastTokenHasBeenQuoted || current.length() != 0) {
            result.add(current.toString());
        }
        if (state == inQuote || state == inDoubleQuote) {
            throw new ParseException("unbalanced quotes in " + toProcess);
        }
        return result.toArray(new String[result.size()]);
    }

}

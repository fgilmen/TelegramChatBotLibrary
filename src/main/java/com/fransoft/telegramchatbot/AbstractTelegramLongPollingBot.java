package com.fransoft.telegramchatbot;

import com.fransoft.telegramchatbot.bo.CommandData;
import com.fransoft.telegramchatbot.bo.TelegramCommand;
import com.fransoft.telegramchatbot.exception.TelegramException;
import com.fransoft.telegramchatbot.handle.ITelegramBootErrorHandler;
import com.fransoft.telegramchatbot.handle.impl.Slf4jErrTelegramBootErrorHandler;
import com.fransoft.telegramchatbot.utils.CommandLineUtils;
import com.google.common.base.Splitter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import jakarta.annotation.Nullable;
import jakarta.annotation.PostConstruct;
import org.apache.commons.lang3.StringUtils;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.polls.SendPoll;
import org.telegram.telegrambots.meta.api.methods.send.SendAudio;
import org.telegram.telegrambots.meta.api.methods.send.SendDice;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import static org.apache.commons.lang3.ObjectUtils.isEmpty;

/**
 * @author fgilmen@gmail.com
 *
 */
public abstract class AbstractTelegramLongPollingBot extends TelegramLongPollingBot implements Sendable {

    private final String botUsername;
    private final String apiKey;
    private final String botFunction;

    private final Map<String, CommandData> commands = new HashMap<>();
    private final Set<String> restrictedUsers = new HashSet<>();
    private final ITelegramBootErrorHandler errorHandler;


    public static final String COMMAND_HELP = "help";
    
    protected AbstractTelegramLongPollingBot(String botUsername, String apiKey, String botFunction) {
    	this(botUsername, apiKey, botFunction, null);
    }
    
    protected AbstractTelegramLongPollingBot(String botUsername, String apiKey, String botFunction, @Nullable ITelegramBootErrorHandler errorHandler)
    {
    	this.botUsername = botUsername;
    	this.apiKey = apiKey;
    	this.botFunction = botFunction;
  		this.errorHandler = Objects.isNull(errorHandler) ? new Slf4jErrTelegramBootErrorHandler() : errorHandler;
    }
    

    @PostConstruct
    private void init() throws TelegramApiException {
        final var telegramBotsApi = createTelegramBotsApi();
        telegramBotsApi.registerBot(this);
    }
    
    TelegramBotsApi createTelegramBotsApi() throws TelegramApiException {
    	return new TelegramBotsApi(DefaultBotSession.class);
    }
    
    protected abstract void onReceived(Update update, TelegramCommand tc);
   
    @Override
    public void onUpdateReceived(Update update) {
        var tc = TelegramCommand.build(update);
        if (!this.restrictedUser(update)){
            this.sendMessage(tc.sendMessage("no valid user", false));
            return;
        }
        if (Objects.isNull(tc.getVerb())){
            return;
        }
        try {
        	onReceived(update, tc);
        } catch (Exception e) {
        	errorHandler.handleError(e);
        }
    }
    
    @Override
    public String getBotUsername() {
        return this.botUsername;
    }

    @Override
    public String getBotToken() {
        return this.apiKey;
    }
    
    @Override
    public String getBotFunction() {
        return this.botFunction;
    }
    
    public void addCommand(String key, CommandData data) {
    	this.commands.put(key,  data);
    }
    
    @Override
    public CommandData getCommand(String key) {
    	return this.commands.get(key);
    }
    
    public void setRestrictedUsers(String restrictedUsers) {
    	this.restrictedUsers.clear();
    	if (StringUtils.isNotBlank(restrictedUsers)) {
            this.restrictedUsers.addAll(Splitter.on(',')
    	        .trimResults()
    	        .splitToList(restrictedUsers));
    	}
    }
    
    @Override
    public Message sendMessage(SendMessage sendMessage) throws TelegramException {
        try {
           return execute(sendMessage);
        } catch (TelegramApiException e) {
        	throw new TelegramException("sendMessage error", e);
        }
    }

    @Override
    public Message sendPoll(SendPoll sendPoll) throws TelegramException {
        Message message = null;
        try {
            message = execute(sendPoll);
        } catch (TelegramApiException e) {
            throw new TelegramException("sendPoll error", e);
        }
        return message;
    }

    @Override
    public Message sendDocument(SendDocument sendDocument) throws TelegramException {
        try {
            return execute(sendDocument);
        } catch (TelegramApiException e) {
        	throw new TelegramException("sendDocument error", e);
        }

    }

    @Override
    public Message sendAudio(SendAudio sendAudio) throws TelegramException {
        try {
            return execute(sendAudio);
        } catch (TelegramApiException e) {
        	throw new TelegramException("sendAudio error", e);
        }

    }

    @Override
    public Message sendDice(SendDice sendDice) throws TelegramException {
        try {
        	return execute(sendDice);
        } catch (TelegramApiException e) {
        	throw new TelegramException("sendDice error", e);
        }

    }

    public Message printMessage(TelegramCommand tc, String msg) throws TelegramException {
        try {
            return execute(tc.sendMessage(msg, false));
        } catch (TelegramApiException e) {
        	throw new TelegramException("printMessage error", e);
        }
    }

    protected Message printHelp(TelegramCommand tc) throws TelegramException {
        String line = this.botFunction.concat("\n");
        if (!this.commands.isEmpty()) {
            for (CommandData cd:this.commands.values()){
                line = line.concat(CommandLineUtils.getHelpCommandLine(cd.getVerb(), cd.getOptions(), cd.getPresentation()));
            }
        }
        return printMessage(tc, line);
    }

    protected Message noImplement(TelegramCommand tc) throws TelegramException {
        return printMessage(tc, "option no implemented");
    }

    protected boolean restrictedUser(Update update) {
        if (isEmpty(this.restrictedUsers) || this.restrictedUsers.isEmpty()){
            return true;
        }
        return this.restrictedUsers.contains(String.valueOf(update.getMessage().getFrom().getUserName()));
    }


}

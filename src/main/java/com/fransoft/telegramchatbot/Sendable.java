package com.fransoft.telegramchatbot;

import com.fransoft.telegramchatbot.bo.CommandData;
import com.fransoft.telegramchatbot.exception.TelegramException;
import org.telegram.telegrambots.meta.api.methods.polls.SendPoll;
import org.telegram.telegrambots.meta.api.methods.send.SendAudio;
import org.telegram.telegrambots.meta.api.methods.send.SendDice;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

public interface Sendable {
    String getBotUsername();

    String getBotToken();

    String getBotFunction();

    CommandData getCommand(String key);

    Message sendMessage(SendMessage sendMessage) throws TelegramException;

    Message sendPoll(SendPoll sendPoll) throws TelegramException;

    Message sendDocument(SendDocument sendDocument) throws TelegramException;

    Message sendAudio(SendAudio sendAudio) throws TelegramException;

    Message sendDice(SendDice sendDice) throws TelegramException;
}

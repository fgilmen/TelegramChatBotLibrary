package com.fransoft.telegramchatbot;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.fransoft.telegramchatbot.bo.TelegramCommand;
import com.fransoft.telegramchatbot.exception.TelegramException;
import com.fransoft.telegramchatbot.handle.ITelegramBootErrorHandler;
import java.util.stream.Stream;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.polls.SendPoll;
import org.telegram.telegrambots.meta.api.methods.send.SendAudio;
import org.telegram.telegrambots.meta.api.methods.send.SendDice;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@ExtendWith(MockitoExtension.class)
class AbstractTelegramLongPollingBotTest {

  @Mock private ITelegramBootErrorHandler errorHandler;

  @InjectMocks @Spy MockTelegramLongPollingBot bot;

  @Test
  void constructWithoutErrorHandlerThenOk() throws TelegramApiException {
    assertNotNull(new MockTelegramLongPollingBot());
  }

  @Test
  void createTelegramBotsApiThenOk() throws TelegramApiException {
    assertNotNull(bot.createTelegramBotsApi());
  }

  @Test
  void initThenOk() throws TelegramApiException, ReflectiveOperationException {
    // Arrange
    var botsApi = mock(TelegramBotsApi.class);
    doReturn(botsApi).when(bot).createTelegramBotsApi();
    // Act & Assert
    var postConstruct = AbstractTelegramLongPollingBot.class.getDeclaredMethod("init");
    postConstruct.setAccessible(true);
    assertDoesNotThrow(() -> postConstruct.invoke(bot));
  }

  @Test
  void getBotUsernameThenOk() {
    assertEquals(MockTelegramLongPollingBot.USERNAME, bot.getBotUsername());
  }

  @Test
  void getBotTokenThenOk() {
    assertEquals(MockTelegramLongPollingBot.API_KEY, bot.getBotToken());
  }

  @Test
  void getBotFunctionThenOk() {
    assertEquals(MockTelegramLongPollingBot.FUNCTION, bot.getBotFunction());
  }

  @Test
  void getCommandThenOk() {
    // Arrange
    var key = "g";
    var command = TelegramTestHelper.createCommandData(MockTelegramLongPollingBot.FUNCTION);
    bot.addCommand(key, command);
    // Act & Assert
    assertNull(bot.getCommand(key + "jjj"));
    var result = bot.getCommand(key);
    assertEquals(command, result);
  }

  @Test
  void onUpdateReceivedThenOk() throws Exception {
    // Arrange
    var update = TelegramTestHelper.createUpdate("/help -v");
    doReturn(true).when(bot).restrictedUser(update);
    doNothing().when(bot).onReceived(eq(update), any());
    // Act & Assert
    assertDoesNotThrow(() -> bot.onUpdateReceived(update));
    verify(bot, times(1)).onReceived(any(), any());
  }

  @Test
  void onUpdateReceivedWhenExceptionThenHandleException() throws Exception {
    // Arrange
    var ex = new IllegalArgumentException();
    var update = TelegramTestHelper.createUpdate("/help -v");
    doReturn(true).when(bot).restrictedUser(update);
    doThrow(ex).when(bot).onReceived(eq(update), any());
    // Act & Assert
    assertDoesNotThrow(() -> bot.onUpdateReceived(update));
    verify(bot, times(1)).onReceived(any(), any());
    verify(errorHandler, times(1)).handleError(any());
  }

  @Test
  void onUpdateReceivedWhenNoVerbThenDoNothing() throws Exception {
    // Arrange
    var update = TelegramTestHelper.createUpdate("help");
    doReturn(true).when(bot).restrictedUser(update);
    // Act & Assert
    assertDoesNotThrow(() -> bot.onUpdateReceived(update));
    verify(bot, times(0)).onReceived(any(), any());
  }

  @Test
  void onUpdateReceivedWhenUserRestrictedFailThenMessageError() throws Exception {
    // Arrange
    var update = TelegramTestHelper.createUpdate("/help -v");
    doReturn(false).when(bot).restrictedUser(update);
    doReturn(new Message()).when(bot).sendMessage(any());
    // Act & Assert
    assertDoesNotThrow(() -> bot.onUpdateReceived(update));
    verify(bot, times(0)).onReceived(any(), any());
    verify(bot, times(1)).sendMessage(any());
  }

  @Test
  void sendMessageThenOk() throws TelegramApiException {
    // Arrange
    var param = new SendMessage();
    var execResult = new Message();
    doReturn(execResult).when(bot).execute(param);
    // Act & Assert
    var result = bot.sendMessage(param);
    assertEquals(execResult, result);
    verify(bot, times(1)).execute(any(param.getClass()));
  }

  @Test
  void sendMessageWhenTelegramApiExceptionThenTelegramException() throws TelegramApiException {
    // Arrange
    var param = new SendMessage();
    var ex = new TelegramApiException(RandomStringUtils.random(10));
    doThrow(ex).when(bot).execute(param);
    // Act & Assert
    var result = assertThrows(TelegramException.class, () -> bot.sendMessage(param));
    assertEquals(ex, result.getCause());
    verify(bot, times(1)).execute(any(param.getClass()));
  }

  @Test
  void sendMessageWhenOtherExceptionThenOtherException() throws TelegramApiException {
    // Arrange
    var param = new SendMessage();
    var ex = new IllegalArgumentException(RandomStringUtils.random(10));
    doThrow(ex).when(bot).execute(param);
    // Act & Assert
    var result = assertThrows(ex.getClass(), () -> bot.sendMessage(param));
    assertEquals(ex, result);
    verify(bot, times(1)).execute(any(param.getClass()));
  }

  @Test
  void sendPollThenOk() throws TelegramApiException {
    // Arrange
    var param = new SendPoll();
    var execResult = new Message();
    doReturn(execResult).when(bot).execute(param);
    // Act & Assert
    var result = bot.sendPoll(param);
    assertEquals(execResult, result);
    verify(bot, times(1)).execute(any(param.getClass()));
  }

  @Test
  void sendPollWhenTelegramApiExceptionThenTelegramException() throws TelegramApiException {
    // Arrange
    var param = new SendPoll();
    var ex = new TelegramApiException(RandomStringUtils.random(10));
    doThrow(ex).when(bot).execute(param);
    // Act & Assert
    var result = assertThrows(TelegramException.class, () -> bot.sendPoll(param));
    assertEquals(ex, result.getCause());
    verify(bot, times(1)).execute(any(param.getClass()));
  }

  @Test
  void sendPollWhenOtherExceptionThenOtherException() throws TelegramApiException {
    // Arrange
    var param = new SendPoll();
    var ex = new IllegalArgumentException(RandomStringUtils.random(10));
    doThrow(ex).when(bot).execute(param);
    // Act & Assert
    var result = assertThrows(ex.getClass(), () -> bot.sendPoll(param));
    assertEquals(ex, result);
    verify(bot, times(1)).execute(any(param.getClass()));
  }

  @Test
  void sendDocumentThenOk() throws TelegramApiException {
    // Arrange
    var param = new SendDocument();
    var execResult = new Message();
    doReturn(execResult).when(bot).execute(param);
    // Act & Assert
    var result = bot.sendDocument(param);
    assertEquals(execResult, result);
    verify(bot, times(1)).execute(any(param.getClass()));
  }

  @Test
  void sendDocumentWhenTelegramApiExceptionThenTelegramException() throws TelegramApiException {
    // Arrange
    var param = new SendDocument();
    var ex = new TelegramApiException(RandomStringUtils.random(10));
    doThrow(ex).when(bot).execute(param);
    // Act & Assert
    var result = assertThrows(TelegramException.class, () -> bot.sendDocument(param));
    assertEquals(ex, result.getCause());
    verify(bot, times(1)).execute(any(param.getClass()));
  }

  @Test
  void sendDocumentWhenOtherExceptionThenOtherException() throws TelegramApiException {
    // Arrange
    var param = new SendDocument();
    var ex = new IllegalArgumentException(RandomStringUtils.random(10));
    doThrow(ex).when(bot).execute(param);
    // Act & Assert
    var result = assertThrows(ex.getClass(), () -> bot.sendDocument(param));
    assertEquals(ex, result);
    verify(bot, times(1)).execute(any(param.getClass()));
  }

  @Test
  void sendAudioThenOk() throws TelegramApiException {
    // Arrange
    var param = new SendAudio();
    var execResult = new Message();
    doReturn(execResult).when(bot).execute(param);
    // Act & Assert
    var result = bot.sendAudio(param);
    assertEquals(execResult, result);
    verify(bot, times(1)).execute(any(param.getClass()));
  }

  @Test
  void sendAudioWhenTelegramApiExceptionThenTelegramException() throws TelegramApiException {
    // Arrange
    var param = new SendAudio();
    var ex = new TelegramApiException(RandomStringUtils.random(10));
    doThrow(ex).when(bot).execute(param);
    // Act & Assert
    var result = assertThrows(TelegramException.class, () -> bot.sendAudio(param));
    assertEquals(ex, result.getCause());
    verify(bot, times(1)).execute(any(param.getClass()));
  }

  @Test
  void sendAudioWhenOtherExceptionThenOtherException() throws TelegramApiException {
    // Arrange
    var param = new SendAudio();
    var ex = new IllegalArgumentException(RandomStringUtils.random(10));
    doThrow(ex).when(bot).execute(param);
    // Act & Assert
    var result = assertThrows(ex.getClass(), () -> bot.sendAudio(param));
    assertEquals(ex, result);
    verify(bot, times(1)).execute(any(param.getClass()));
  }

  @Test
  void sendDiceThenOk() throws TelegramApiException {
    // Arrange
    var param = new SendDice();
    var execResult = new Message();
    doReturn(execResult).when(bot).execute(param);
    // Act & Assert
    var result = bot.sendDice(param);
    assertEquals(execResult, result);
    verify(bot, times(1)).execute(any(param.getClass()));
  }

  @Test
  void sendDiceWhenTelegramApiExceptionThenTelegramException() throws TelegramApiException {
    // Arrange
    var param = new SendDice();
    var ex = new TelegramApiException(RandomStringUtils.random(10));
    doThrow(ex).when(bot).execute(param);
    // Act & Assert
    var result = assertThrows(TelegramException.class, () -> bot.sendDice(param));
    assertEquals(ex, result.getCause());
    verify(bot, times(1)).execute(any(param.getClass()));
  }

  @Test
  void sendDiceWhenOtherExceptionThenOtherException() throws TelegramApiException {
    // Arrange
    var param = new SendDice();
    var ex = new IllegalArgumentException(RandomStringUtils.random(10));
    doThrow(ex).when(bot).execute(param);
    // Act & Assert
    var result = assertThrows(ex.getClass(), () -> bot.sendDice(param));
    assertEquals(ex, result);
    verify(bot, times(1)).execute(any(param.getClass()));
  }

  @Test
  void printMessageThenOk() throws TelegramApiException {
    // Arrange
    var text = "Hello Mortimer";
    var param =
        TelegramCommand.build(TelegramTestHelper.createUpdate(RandomStringUtils.random(10)));
    var execResult = new Message();
    doReturn(execResult).when(bot).execute(any(SendMessage.class));
    // Act & Assert
    var result = bot.printMessage(param, text);
    assertEquals(execResult, result);
    verify(bot, times(1)).execute(any(SendMessage.class));
  }

  @Test
  void printMessageWhenTelegramApiExceptionThenTelegramException() throws TelegramApiException {
    // Arrange
    var text = "Hello Mortimer";
    var param =
        TelegramCommand.build(TelegramTestHelper.createUpdate(RandomStringUtils.random(10)));
    var ex = new TelegramApiException(RandomStringUtils.random(10));
    doThrow(ex).when(bot).execute(any(SendMessage.class));
    // Act & Assert
    var result = assertThrows(TelegramException.class, () -> bot.printMessage(param, text));
    assertEquals(ex, result.getCause());
    verify(bot, times(1)).execute(any(SendMessage.class));
  }

  @Test
  void printMessageWhenOtherExceptionThenOtherException() throws TelegramApiException {
    // Arrange
    var text = "Hello Mortimer";
    var param =
        TelegramCommand.build(TelegramTestHelper.createUpdate(RandomStringUtils.random(10)));
    var ex = new IllegalArgumentException(RandomStringUtils.random(10));
    doThrow(ex).when(bot).execute(any(SendMessage.class));
    // Act & Assert
    var result = assertThrows(ex.getClass(), () -> bot.printMessage(param, text));
    assertEquals(ex, result);
    verify(bot, times(1)).execute(any(SendMessage.class));
  }

  @Test
  void printHelpThenOk() throws TelegramApiException {
    // Arange
    bot.addCommand("f", TelegramTestHelper.createCommandData(MockTelegramLongPollingBot.FUNCTION));
    var param =
        TelegramCommand.build(TelegramTestHelper.createUpdate(RandomStringUtils.random(10)));
    var execResult = new Message();
    doReturn(execResult).when(bot).printMessage(eq(param), any());
    // Act & Assert
    var result = bot.printHelp(param);
    assertEquals(execResult, result);
    verify(bot, times(1)).printMessage(any(), any());
  }

  @Test
  void printHelpWhenNoCommandsThenOk() throws TelegramApiException {
    // Arange
    var param =
        TelegramCommand.build(TelegramTestHelper.createUpdate(RandomStringUtils.random(10)));
    var execResult = new Message();
    doReturn(execResult).when(bot).printMessage(eq(param), any());
    // Act & Assert
    var result = bot.printHelp(param);
    assertEquals(execResult, result);
    verify(bot, times(1)).printMessage(any(), any());
  }

  @Test
  void noImplementThenOk() throws TelegramApiException {
    // Arrange
    var param =
        TelegramCommand.build(TelegramTestHelper.createUpdate(RandomStringUtils.random(10)));
    var execResult = new Message();
    doReturn(execResult).when(bot).printMessage(eq(param), any());
    // Act & Assert
    var result = bot.noImplement(param);
    assertEquals(execResult, result);
    verify(bot, times(1)).printMessage(any(), any());
  }

  @ParameterizedTest
  @MethodSource("provideRestrictedUser")
  void restrictedUserTest(String botRestrictedUsers, boolean expected) {
    // Arrange
    bot.setRestrictedUsers(botRestrictedUsers);
    var update = TelegramTestHelper.createUpdate("Example");
    // Act & Assert
    var result = bot.restrictedUser(update);
    assertEquals(expected, result);
  }

  private static Stream<Arguments> provideRestrictedUser() {
    return Stream.of(
        Arguments.of("", true),
        Arguments.of(TelegramTestHelper.USER_AUTH, true),
        Arguments.of("unkown.user", false));
  }

  static class MockTelegramLongPollingBot extends AbstractTelegramLongPollingBot {

    private static final String API_KEY = "API_KEY";
    private static final String USERNAME = "USERNAME";
    private static final String FUNCTION = "help";

    public MockTelegramLongPollingBot(ITelegramBootErrorHandler errorHandler) throws TelegramApiException {
      super(USERNAME, API_KEY, FUNCTION, errorHandler);
    }

    public MockTelegramLongPollingBot() throws TelegramApiException {
      super(USERNAME, API_KEY, FUNCTION);
    }

    @Override
    protected void onReceived(Update update, TelegramCommand tc) {}
  }
}

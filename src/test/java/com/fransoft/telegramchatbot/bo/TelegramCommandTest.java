package com.fransoft.telegramchatbot.bo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import com.fransoft.telegramchatbot.TelegramTestHelper;
import com.fransoft.telegramchatbot.utils.CommandLineUtils;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.objects.InputFile;

@ExtendWith(MockitoExtension.class)
class TelegramCommandTest {

  @Mock private MockedStatic<CommandLineUtils> cmdLineUtils;

  @Test
  void buildWithNoUpdateThenNullPointerException() {
    // Act & Assert
    var exResult = assertThrows(NullPointerException.class, () -> TelegramCommand.build(null));
    assertTrue(exResult.getMessage().startsWith("update"));
  }

  @Test
  void buildWithCommandAndNoOptionsThenOk() {
    // Arrange
    var text = "hello";
    var update = TelegramTestHelper.createUpdate("/" + text);
    // Act & Assert
    var result = TelegramCommand.build(update);
    assertNotNull(result);
    assertEquals(String.valueOf(TelegramTestHelper.CHAT_ID), result.getChatId());
    assertEquals(TelegramTestHelper.USER_ID, result.getUserId());
    assertEquals(text, result.getVerb());
    assertEquals(true, result.isCommand());
    assertEquals(null, result.getParams());
  }

  @Test
  void buildWithNoCommandAndNoOptionsThenOk() {
    // Arrange
    var text = "hello";
    var update = TelegramTestHelper.createUpdate(text);
    // Act & Assert
    var result = TelegramCommand.build(update);
    assertNotNull(result);
    assertEquals(String.valueOf(TelegramTestHelper.CHAT_ID), result.getChatId());
    assertEquals(TelegramTestHelper.USER_ID, result.getUserId());
    assertEquals(null, result.getVerb());
    assertEquals(false, result.isCommand());
    assertEquals(null, result.getParams());
  }

  @Test
  void buildWithNoTextThenOk() throws ParseException {
    // Arrange
    var text = "";
    var update = TelegramTestHelper.createUpdate(text);
    var options = TelegramTestHelper.createOptions();
    // Act & Assert
    var result = TelegramCommand.build(update, options);
    assertNotNull(result);
    assertEquals(String.valueOf(TelegramTestHelper.CHAT_ID), result.getChatId());
    assertEquals(TelegramTestHelper.USER_ID, result.getUserId());
    assertEquals(null, result.getVerb());
    assertEquals(false, result.isCommand());
    assertEquals(null, result.getParams());
  }

  @Test
  void buildWithCommandThenOk() throws ParseException {
    // Arrange
    var text = "hello";
    var update = TelegramTestHelper.createUpdate("/" + text);
    var options = TelegramTestHelper.createOptions();
    var commandExpected = new CommandLine.Builder().build();
    cmdLineUtils
        .when(() -> CommandLineUtils.getCommandLine(any(), any()))
        .thenReturn(commandExpected);
    // Act & Assert
    var result = TelegramCommand.build(update, options);
    assertNotNull(result);
    assertEquals(String.valueOf(TelegramTestHelper.CHAT_ID), result.getChatId());
    assertEquals(TelegramTestHelper.USER_ID, result.getUserId());
    assertEquals(text, result.getVerb());
    assertEquals(true, result.isCommand());
    assertEquals(commandExpected, result.getParams());
  }

  @Test
  void buildWithCallbackQueryThenOk() throws ParseException {
    // Arrange
    var text = "hello";
    var update = TelegramTestHelper.createUpdateCallbackQuery("/" + text);
    var options = TelegramTestHelper.createOptions();
    var commandExpected = new CommandLine.Builder().build();
    cmdLineUtils
        .when(() -> CommandLineUtils.getCommandLine(any(), any()))
        .thenReturn(commandExpected);
    // Act & Assert
    var result = TelegramCommand.build(update, options);
    assertNotNull(result);
    assertEquals(String.valueOf(TelegramTestHelper.CHAT_ID), result.getChatId());
    assertEquals(TelegramTestHelper.USER_ID, result.getUserId());
    assertEquals(text, result.getVerb());
    assertEquals(true, result.isCommand());
    assertEquals(commandExpected, result.getParams());
  }

  @ParameterizedTest
  @CsvSource(
      emptyValue = "EMPTY",
      value = {"EMPTY,true", "test,true", "test,false"})
  void sendMessageThenOk(String text, boolean html) {
    // Arrange
    var update = TelegramTestHelper.createUpdate("/command");
    var cmd = TelegramCommand.build(update);
    // Act & Assert
    var result = cmd.sendMessage(text, html);
    assertEquals(cmd.getChatId(), result.getChatId());
    assertEquals(text, result.getText());
    assertEquals(html ? ParseMode.HTML : null, result.getParseMode());
  }

  @Test
  void sendMessageWithNullTextThenNullPointerException() {
    // Arrange
    var update = TelegramTestHelper.createUpdate("/command");
    var cmd = TelegramCommand.build(update);
    // Act & Assert
    var exResult = assertThrows(NullPointerException.class, () -> cmd.sendMessage(null, true));
    assertTrue(exResult.getMessage().startsWith("text"));
  }

  @ParameterizedTest
  @CsvSource(
      emptyValue = "EMPTY",
      value = {"EMPTY", "test"})
  void sendDocThenOk(String content) {
    // Arrange
    var update = TelegramTestHelper.createUpdate("/command");
    var cmd = TelegramCommand.build(update);
    var input = new InputFile(content);
    // Act & Assert
    var result = cmd.sendDoc(input);
    assertEquals(cmd.getChatId(), result.getChatId());
    assertEquals(input, result.getDocument());
  }

  @Test
  void sendDocWithNullDocumentThenNullPointerException() {
    // Arrange
    var update = TelegramTestHelper.createUpdate("/command");
    var cmd = TelegramCommand.build(update);
    // Act & Assert
    var exResult = assertThrows(NullPointerException.class, () -> cmd.sendDoc(null));
    assertTrue(exResult.getMessage().startsWith("document"));
  }

  @ParameterizedTest
  @CsvSource(
      nullValues = "NULL",
      emptyValue = "EMPTY",
      value = {"NULL", "EMPTY", "test"})
  void sendDiceThenOk(String emoji) {
    // Arrange
    var update = TelegramTestHelper.createUpdate("/command");
    var cmd = TelegramCommand.build(update);
    // Act & Assert
    var result = cmd.sendDice(emoji);
    assertEquals(cmd.getChatId(), result.getChatId());
    assertEquals(emoji, result.getEmoji());
  }
}

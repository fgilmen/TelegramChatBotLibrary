package com.fransoft.telegramchatbot;

import com.fransoft.telegramchatbot.bo.CommandData;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;

public class TelegramTestHelper {

  public static final Long CHAT_ID = 1L;
  public static final Long USER_ID = 9L;
  public static final String USER_AUTH = "john.doe";

  public static Update createUpdate(String text) {
    return createUpdate(text, USER_AUTH);
  }

  public static Update createUpdate(String text, String username) {
    var chat = new Chat(CHAT_ID, "private");
    var user = new User();
    user.setUserName(username);
    user.setId(USER_ID);
    var msg = new Message();
    msg.setText(text);
    msg.setChat(chat);
    msg.setFrom(user);
    var update = new Update();
    update.setMessage(msg);
    return update;
  }

  public static Update createUpdateCallbackQuery(String text) {
    return createUpdateCallbackQuery(text, USER_AUTH);
  }

  public static Update createUpdateCallbackQuery(String text, String username) {
    var chat = new Chat(CHAT_ID, "private");
    var user = new User();
    user.setUserName(username);
    user.setId(USER_ID);
    var msg = new Message();
    msg.setChat(chat);
    msg.setFrom(user);
    var callback = new CallbackQuery();
    callback.setData(text);
    callback.setMessage(msg);
    var update = new Update();
    update.setCallbackQuery(callback);
    return update;
  }

  public static CommandData createCommandData(String botFunction, Options options) {
    return CommandData.builder().verb(botFunction).options(options).build();
  }

  public static CommandData createCommandData(String botFunction) {
    return CommandData.builder().verb(botFunction).options(createOptions()).build();
  }

  /*
   * -f,--file <FILE>          The file to be processed
   * -h,--help
   * -v,--version              Print the version of the application
   */
  public static Options createOptions() {
    var options = new Options();
    options.addOption(
        Option.builder("f")
            .longOpt("file")
            .desc("The file to be processed")
            .hasArg()
            .argName("FILE")
            .required()
            .build());
    options.addOption(
        Option.builder("v")
            .longOpt("version")
            .desc("Print the version of the application")
            .build());
    options.addOption(Option.builder("h").longOpt("help").build());
    return options;
  }
}

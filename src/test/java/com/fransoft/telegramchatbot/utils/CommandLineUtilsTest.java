package com.fransoft.telegramchatbot.utils;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fransoft.telegramchatbot.TelegramTestHelper;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class CommandLineUtilsTest {

  @Test
  void getHelpCommandLineThenOk() {
    // Arrange
    var verb = "test";
    var options = TelegramTestHelper.createOptions();
    var header = "HEADER Test";
    var expected =
        "usage: test"
            + System.lineSeparator()
            + "HEADER Test"
            + System.lineSeparator()
            + "          -f,--file <FILE>          The file to be processed"
            + System.lineSeparator()
            + "          -h,--help"
            + System.lineSeparator()
            + "          -v,--version              Print the version of the application";
    // Act & Assert
    var result = CommandLineUtils.getHelpCommandLine(verb, options, header);
    assertTrue(StringUtils.isNotBlank(result));
    assertEquals(expected, result.trim());
  }

  @Test
  void getHelpCommandLineWithoutOptionsThenOk() {
    // Arrange
    var verb = "test";
    var header = "HEADER Test";
    var expected = "usage: test" + System.lineSeparator() + "HEADER Test";
    // Act & Assert
    var result = CommandLineUtils.getHelpCommandLine(verb, null, header);
    assertTrue(StringUtils.isNotBlank(result));
    assertEquals(expected, result.trim());
  }

  @Test
  void getHelpCommandLineWithoutHeaderThenOk() {
    // Arrange
    var verb = "test";
    var expected = "usage: test";
    // Act & Assert
    var result = CommandLineUtils.getHelpCommandLine(verb, null, null);
    assertTrue(StringUtils.isNotBlank(result));
    assertEquals(expected, result.trim());
  }

  @ParameterizedTest
  @ValueSource(
      strings = {
        "-f Arg0",
        "-f \"Arg0\"",
        "-f ' Arg0'",
        "--file Arg0",
        "--file \"Arg0 \"",
        "--file ' Arg0 '"
      })
  void getCommandLineThenOk(String requiredArg) throws ParseException {
    // Arrange
    var strCmd = "test " + requiredArg;
    var options = TelegramTestHelper.createOptions();
    // Act & Assert
    var result = CommandLineUtils.getCommandLine(strCmd, options);
    assertNotNull(result);
    assertArrayEquals(new String[] {"test"}, result.getArgs());
    assertEquals(1, result.getOptions().length);
    assertEquals("Arg0", result.getOptionValue("f").trim());
  }

  @ParameterizedTest
  @ValueSource(strings = {"-f ''", "-f \"\"", "-f ' '", "-f \" \""})
  // TODO esto es lo que queremos?
  void getCommandLineWhenRequiredArgIsEmptyThenOk(String param) throws ParseException {
    // Arrange
    var strCmd = "test " + param;
    var options = TelegramTestHelper.createOptions();
    // Act & Assert
    var result = CommandLineUtils.getCommandLine(strCmd, options);
    assertNotNull(result);
    assertArrayEquals(new String[] {"test"}, result.getArgs());
    assertEquals(1, result.getOptions().length);
    assertEquals("", result.getOptionValue("f").trim());
  }

  @Test
  void getCommandLineWhenNoStrCommandThenOk() throws ParseException {
    // Arrange
    var options = new Options();
    options.addOption(
        Option.builder("v")
            .longOpt("version")
            .desc("Print the version of the application")
            .build());
    // Act & Assert
    var result = CommandLineUtils.getCommandLine(null, options);
    assertNotNull(result);
    assertEquals(0, result.getArgs().length);
    assertEquals(0, result.getOptions().length);
  }

  @Test
  void getCommandLineWhenNotRequiredOptionThenParseException() throws ParseException {
    // Arrange
    var strCmd = "test";
    var options = TelegramTestHelper.createOptions();
    // Act & Assert
    assertThrows(
        MissingOptionException.class, () -> CommandLineUtils.getCommandLine(strCmd, options));
  }

  @ParameterizedTest
  @ValueSource(strings = {"-f", "--file"})
  void getCommandLineWhenNotRequiredArgThenParseException(String param) throws ParseException {
    // Arrange
    var strCmd = "test " + param;
    var options = TelegramTestHelper.createOptions();
    // Act & Assert
    assertThrows(
        MissingArgumentException.class, () -> CommandLineUtils.getCommandLine(strCmd, options));
  }

  @ParameterizedTest
  @ValueSource(strings = {"-f \"ss", "-f ss\"", "-f 'ss", "-f ss'"})
  void getCommandLineWhenArgQuotesUnbalancedThenParseException(String param) throws ParseException {
    // Arrange
    var strCmd = "test " + param;
    var options = TelegramTestHelper.createOptions();
    // Act & Assert
    assertThrows(ParseException.class, () -> CommandLineUtils.getCommandLine(strCmd, options));
  }
}
